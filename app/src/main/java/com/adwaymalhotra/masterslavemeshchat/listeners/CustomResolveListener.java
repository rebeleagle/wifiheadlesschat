package com.adwaymalhotra.masterslavemeshchat.listeners;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;
import com.adwaymalhotra.masterslavemeshchat.util.Constants;

/**
 * Created by Adway on 7/31/2016.
 */
public class CustomResolveListener implements NsdManager.ResolveListener {
  private static final String TAG = "CustomResolveListener";
  private final String mServiceName;
  private final Callback callback;

  public CustomResolveListener(String mServiceName, Callback callback) {
    this.mServiceName = mServiceName;
    this.callback = callback;
  }

  @Override public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int errorCode) {
    Log.d(TAG, "onResolveFailed: " + nsdServiceInfo);
    Log.d(TAG, "onResolveFailed: error code " + errorCode);
  }

  @Override public void onServiceResolved(NsdServiceInfo serviceInfo) {
//if self
    if (serviceInfo.getServiceName().equalsIgnoreCase(mServiceName)) {
      Log.d(TAG, "onServiceResolved: self");
      return;
    }

    NodeProfile nodeProfile = new NodeProfile();
    nodeProfile.setServiceName(serviceInfo.getServiceName());
    nodeProfile.setServicePort(serviceInfo.getPort());
    nodeProfile.setServiceHost(serviceInfo.getHost());
    if (nodeProfile.getServiceName()
        .contains(Constants.UserMode.getString(Constants.UserMode.MASTER))) {
      nodeProfile.setUserMode(Constants.UserMode.MASTER);
    } else if (nodeProfile.getServiceName()
        .contains(Constants.UserMode.getString(Constants.UserMode.SLAVE))) {
      nodeProfile.setUserMode(Constants.UserMode.SLAVE);
    }
    nodeProfile.setNodeName(nodeProfile.getServiceName().split("#")[0]);

    callback.onServiceResolved(nodeProfile);
  }

  public interface Callback {
    void onServiceResolved(NodeProfile nodeProfile);
  }
}
