package com.adwaymalhotra.masterslavemeshchat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.adwaymalhotra.masterslavemeshchat.R;
import com.adwaymalhotra.masterslavemeshchat.interfaces.ILoginView;
import com.adwaymalhotra.masterslavemeshchat.presenter.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Adway on 7/31/2016.
 */
public class LoginActivity extends AppCompatActivity implements ILoginView {
  private static final String TAG = "LoginActivity";
  @BindView(R.id.txv_username) protected EditText usernameET;
  @BindView(R.id.txv_password) protected EditText passwordET;
  LoginPresenter loginPresenter;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    ButterKnife.bind(this);
    loginPresenter = new LoginPresenter(this);
  }

  @OnClick(R.id.btn_login) public void onLoginClick() {
    Log.d(TAG, "onLoginClick: ");
    loginPresenter.doLogin();
  }

  @Override public String getUsername() {
    return usernameET.getText().toString();
  }

  @Override public String getPassword() {
    return passwordET.getText().toString();
  }

  @Override public void onLoginSuccessful() {
    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
    startActivity(intent);
    finish();
  }

  @Override public void showErrorMessage(String message) {
    Snackbar.make(usernameET, message, Snackbar.LENGTH_SHORT).show();
  }
}
