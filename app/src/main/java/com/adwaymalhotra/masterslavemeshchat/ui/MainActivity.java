package com.adwaymalhotra.masterslavemeshchat.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.adwaymalhotra.masterslavemeshchat.LocalService;
import com.adwaymalhotra.masterslavemeshchat.R;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IChatView;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IContactView;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IParentView;
import com.adwaymalhotra.masterslavemeshchat.interfaces.MainToServiceInterface;
import com.adwaymalhotra.masterslavemeshchat.interfaces.ServiceToMainInterface;
import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;
import com.adwaymalhotra.masterslavemeshchat.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Adway on 7/31/2016.
 */
public class MainActivity extends AppCompatActivity implements IParentView, ServiceToMainInterface {
  private static final String TAG = "MainActivity";

  @BindView(R.id.main_toolbar) protected Toolbar toolbar;

  private IChatView chatView;
  private IContactView contactsView;
  private MainToServiceInterface mainToServiceInterface;
  private boolean isServiceBound = false;

  private ServiceConnection mServiceConnection = new ServiceConnection() {
    @Override public void onServiceConnected(ComponentName name, IBinder service) {
      Log.d(TAG, "onServiceConnected: ");
      isServiceBound = true;
      LocalService.InterfaceBinder binder = (LocalService.InterfaceBinder) service;
      mainToServiceInterface = binder.getServiceInterface();

      binder.setMainInterface(MainActivity.this);

      gotoContactsView();
    }

    @Override public void onServiceDisconnected(ComponentName name) {
      Log.d(TAG, "onServiceDisconnected: ");
      isServiceBound = false;
    }
  };

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
  }

  @Override protected void onStart() {
    super.onStart();
    if (!isServiceBound) {
      Log.d(TAG, "onStart: starting service");
      Intent serviceIntent = new Intent(MainActivity.this, LocalService.class);
      bindService(serviceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    if (isServiceBound) {
      unbindService(mServiceConnection);
    }
  }

  @Override public void initToolbar(String title, String subtitle) {
    toolbar.setTitle(title);
    toolbar.setSubtitle(subtitle);
    toolbar.setNavigationIcon(null);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
  }

  @Override public void initToolbar(String title) {
    toolbar.setTitle(title);
    toolbar.setSubtitle(null);
    toolbar.setNavigationIcon(null);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
  }

  @Override public void initToolbar(String title, String subtitle, boolean showBackButton) {
    toolbar.setTitle(title);
    toolbar.setSubtitle(subtitle);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(showBackButton);
  }

  @Override public void initToolbar(String title, boolean showBackButton) {
    toolbar.setTitle(title);
    toolbar.setSubtitle(null);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(showBackButton);
  }

  @Override public void startChatWith(NodeProfile nodeProfile) {
    Log.d(TAG, "startChatWith: ");
    mainToServiceInterface.connectToNode(nodeProfile);
  }

  @Override public void gotoContactsView() {
    FragmentManager fm = getSupportFragmentManager();
    fm.beginTransaction().replace(R.id.main_container, new ContactListFragment()).commit();
  }

  @Override public ArrayList<ChatMessage> getAllMessagesForNode(NodeProfile nodeProfile) {
    if (mainToServiceInterface != null) {
      return mainToServiceInterface.getAllMessagesForNode(nodeProfile);
    }
    return null;
  }

  @Override public ArrayList<NodeProfile> getAllContacts() {
    if (mainToServiceInterface != null) {
      return mainToServiceInterface.getAllContacts();
    }
    return null;
  }

  @Override public void setChatView(IChatView chatView) {
    this.chatView = chatView;
  }

  @Override public void setContactsView(IContactView contactsView) {
    this.contactsView = contactsView;
  }

  @Override public void refreshContacts() {
    mainToServiceInterface.refreshDiscoveredServices();
  }

  @Override public void sendMessage(String message, NodeProfile nodeProfile) {
    mainToServiceInterface.sendMessage(nodeProfile, message);
  }

  @Override
  public void notifyConnectionStatus(final NodeProfile nodeProfile, final boolean isSuccess) {
    runOnUiThread(new Runnable() {
      @Override public void run() {
        if (isSuccess) {
          Log.d(TAG, "notifyConnectionStatus: successfully connected...");
          FragmentManager fm = getSupportFragmentManager();
          ChatMessagesFragment fragment = new ChatMessagesFragment();
          Bundle bundle = new Bundle();
          bundle.putSerializable(Constants.Keys.NODE, nodeProfile);
          fragment.setArguments(bundle);
          fm.beginTransaction().replace(R.id.main_container, fragment).commit();
        } else {

        }
      }
    });
  }

  @Override public void notifyConnectionsListUpdated(final ArrayList<NodeProfile> list) {
    runOnUiThread(new Runnable() {
      @Override public void run() {
        if (contactsView != null) {
          contactsView.notifyContactListUpdated(list);
        }
      }
    });
  }

  @Override
  public void notifyMessageListUpdated(final NodeProfile nodeProfile,
                                       final ArrayList<ChatMessage> allMessages) {
    runOnUiThread(new Runnable() {
      @Override public void run() {
        if (chatView != null) {
          chatView.updateMessageList(nodeProfile, allMessages);
        }
      }
    });
  }

  @Override public void showSnackbar(final String message) {
    runOnUiThread(new Runnable() {
      @Override public void run() {
        Snackbar.make(toolbar, message, Snackbar.LENGTH_SHORT).show();
      }
    });
  }

  @Override public void showToast(final String message) {
    runOnUiThread(new Runnable() {
      @Override public void run() {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
      }
    });
  }
}
