package com.adwaymalhotra.masterslavemeshchat.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.adwaymalhotra.masterslavemeshchat.R;
import com.adwaymalhotra.masterslavemeshchat.adapters.MessagesRecyclerAdapter;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IChatView;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IParentView;
import com.adwaymalhotra.masterslavemeshchat.listeners.CustomResolveListener;
import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;
import com.adwaymalhotra.masterslavemeshchat.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Adway on 7/31/2016.
 */
public class ChatMessagesFragment extends Fragment implements IChatView {
  private static final String TAG = "ChatMessagesFragment";

  @BindView(R.id.chat_messages_recycler) protected RecyclerView messagesRecycler;
  @BindView(R.id.chat_messages_edit) protected EditText editText;

  private IParentView parentView;
  private NodeProfile currentNode;

  private MessagesRecyclerAdapter adapter;

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_chat_view, container, false);
    ButterKnife.bind(this, view);
    Bundle bundle = getArguments();
    currentNode = (NodeProfile) bundle.getSerializable(Constants.Keys.NODE);
    return view;
  }

  @Override public void onStart() {
    super.onStart();
    parentView.setChatView(this);
    parentView.initToolbar(currentNode.getNodeName(), true);

    initRecyclerView();
  }

  private void initRecyclerView() {
    ArrayList<ChatMessage> allMessages = parentView.getAllMessagesForNode(currentNode);
    if(allMessages == null) allMessages = new ArrayList<>();
    adapter = new MessagesRecyclerAdapter(allMessages);
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    messagesRecycler.setAdapter(adapter);
    messagesRecycler.setLayoutManager(llm);
  }

  @Override public void onStop() {
    super.onStop();
    parentView.setChatView(null);
  }

  @Override
  public void updateMessageList(NodeProfile nodeProfile, ArrayList<ChatMessage> allMessages) {
    Log.d(TAG, "updateMessageList: nodeProfile: " + nodeProfile.getServiceName());
    Log.d(TAG, "updateMessageList: currentNode:" + currentNode.getServiceName());
    if (nodeProfile.equals(currentNode) && adapter != null) {
      adapter.updateDataset(allMessages);
      messagesRecycler.smoothScrollToPosition(allMessages.size());
    }
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    parentView = (IParentView) context;
  }

  @OnClick(R.id.chat_messages_send) public void onSendClick(){
    if(editText.getText().toString().isEmpty()) {
      Toast.makeText(getActivity(), "Please enter some text. ", Toast.LENGTH_SHORT).show();
    } else {
      parentView.sendMessage(editText.getText().toString(), currentNode);
    }
  }
}
