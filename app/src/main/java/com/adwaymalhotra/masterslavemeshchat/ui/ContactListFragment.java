package com.adwaymalhotra.masterslavemeshchat.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.adwaymalhotra.masterslavemeshchat.R;
import com.adwaymalhotra.masterslavemeshchat.adapters.ContactsRecyclerAdapter;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IContactView;
import com.adwaymalhotra.masterslavemeshchat.interfaces.IParentView;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Adway on 7/31/2016.
 */
public class ContactListFragment extends Fragment implements IContactView {
  private static final String TAG = "ContactListFragment";
  private IParentView parentView;

  @BindView(R.id.contacts_recycler) protected RecyclerView contactsRecycler;
  private ContactsRecyclerAdapter adapter;

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_contacts_list, container, false);
    ButterKnife.bind(this, view);
    setHasOptionsMenu(true);
    return view;
  }

  @Override public void onStart() {
    super.onStart();
    initRecycler();
    parentView.setContactsView(this);
    parentView.initToolbar("Available Contacts", false);
  }

  @Override public void onStop() {
    super.onStop();
    parentView.setContactsView(null);
  }

  private void initRecycler() {
    ArrayList<NodeProfile> allNodes = parentView.getAllContacts();
    if (allNodes == null) allNodes = new ArrayList<>();

    adapter = new ContactsRecyclerAdapter(allNodes);
    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    contactsRecycler.setLayoutManager(llm);
    contactsRecycler.setAdapter(adapter);

    //TODO make the touch listener
    final GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener(){
      @Override public boolean onSingleTapUp(MotionEvent e) {
        return true;
      }
    });
    contactsRecycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
      @Override public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        Log.d(TAG, "onInterceptTouchEvent: ");
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if(childView!=null && gestureDetector.onTouchEvent(e)){

          int position = rv.getChildAdapterPosition(childView);
          parentView.startChatWith(adapter.getItemAt(position));
        }
        return false;
      }

      @Override public void onTouchEvent(RecyclerView rv, MotionEvent e) {
      }

      @Override public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

      }
    });
  }

  @Override public void notifyContactListUpdated(ArrayList<NodeProfile> listOfNodes) {
    if (adapter != null) {
      adapter.updateDataset(listOfNodes);
    }
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    parentView = (IParentView) context;
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.fragment_contacts, menu);
    super.onCreateOptionsMenu(menu, inflater);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if(item.getItemId() == android.R.id.home){
      parentView.gotoContactsView();
    } else if(item.getItemId() == R.id.contacts_refresh) {
      parentView.refreshContacts();
    }
    return super.onOptionsItemSelected(item);
  }
}
