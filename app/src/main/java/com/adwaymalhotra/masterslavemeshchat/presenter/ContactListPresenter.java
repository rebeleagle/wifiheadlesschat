package com.adwaymalhotra.masterslavemeshchat.presenter;

import com.adwaymalhotra.masterslavemeshchat.interfaces.IContactView;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

/**
 * Created by Adway on 7/30/2016.
 */
public class ContactListPresenter {

  private final IContactView contactView;

  ContactListPresenter(IContactView contactView) {
    this.contactView = contactView;
  }

  public void startChatWithUser(NodeProfile user) {
    //TODO initialize connection with selected user and wait for response from service.
  }

  public void refreshContactList() {
    //TODO ask service connection to update contact list
  }
}
