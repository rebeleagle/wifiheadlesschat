package com.adwaymalhotra.masterslavemeshchat.presenter;

import android.util.Log;

import com.adwaymalhotra.masterslavemeshchat.interfaces.ILoginView;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;
import com.adwaymalhotra.masterslavemeshchat.util.Constants;

/**
 * Created by Adway on 7/30/2016.
 */
public class LoginPresenter {
  private static final String TAG = "LoginPresenter";
  private ILoginView loginView;

  public LoginPresenter(ILoginView loginView) {
    this.loginView = loginView;
  }

  public void doLogin() {
    Log.d(TAG, "doLogin: ");
    String username = loginView.getUsername(), password = loginView.getPassword();

    if(!username.isEmpty() && !password.isEmpty()) {
      if (username.equalsIgnoreCase("master") && password.equalsIgnoreCase("master")) {
        Constants.currentUserMode = Constants.UserMode.MASTER;
        createOwnNodeProfile(username, Constants.UserMode.MASTER);
        loginView.onLoginSuccessful();
      } else if (username.equalsIgnoreCase("slave") && password.equalsIgnoreCase("slave")) {
        Constants.currentUserMode = Constants.UserMode.SLAVE;
        createOwnNodeProfile(username, Constants.UserMode.SLAVE);
        loginView.onLoginSuccessful();
      } else {
        loginView.showErrorMessage("Authentication Failed!");
      }
    } else {
      loginView.showErrorMessage("Please enter something!");
    }
  }

  private void createOwnNodeProfile(String username, Constants.UserMode userMode) {
    NodeProfile ownProfile = new NodeProfile();
    ownProfile.setNodeName(Constants.DEVICE_NAME);
    ownProfile.setUserMode(userMode);
    ownProfile.setServicePort(Constants.SERVICE_PORT);
    Constants.currentUserMode = userMode;
    Constants.myProfile = ownProfile;
  }
}
