package com.adwaymalhotra.masterslavemeshchat.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Adway on 7/30/2016.
 */
public class ChatMessage implements Serializable, Comparable {
  private NodeProfile sender;
  private NodeProfile receiver;
  private String text;
  private long timestamp;
  private int id;

  public NodeProfile getSender() {
    return sender;
  }

  public void setSender(NodeProfile sender) {
    this.sender = sender;
  }

  public NodeProfile getReceiver() {
    return receiver;
  }

  public void setReceiver(NodeProfile receiver) {
    this.receiver = receiver;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override public int compareTo(Object another) {
    if(another instanceof ChatMessage){
      if(timestamp<((ChatMessage) another).getTimestamp()) return -1;
      else if(timestamp>((ChatMessage) another).getTimestamp()) return 1;
    }
    return 0;
  }
}
