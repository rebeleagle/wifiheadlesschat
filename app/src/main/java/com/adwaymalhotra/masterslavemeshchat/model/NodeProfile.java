package com.adwaymalhotra.masterslavemeshchat.model;

import com.adwaymalhotra.masterslavemeshchat.util.Constants;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Created by Adway on 7/30/2016.
 */
public class NodeProfile implements Serializable {
  private Constants.UserMode userMode;
  private String nodeName;
  private InetAddress serviceHost;
  private int servicePort;
  private String serviceName;
  private int id;

  @Override public boolean equals(Object o) {
    return serviceName.equalsIgnoreCase(((NodeProfile)o).getServiceName());
  }

  public Constants.UserMode getUserMode() {
    return userMode;
  }

  public void setUserMode(Constants.UserMode userMode) {
    this.userMode = userMode;
  }

  public String getNodeName() {
    return nodeName;
  }

  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  public InetAddress getServiceHost() {
    return serviceHost;
  }

  public void setServiceHost(InetAddress serviceHost) {
    this.serviceHost = serviceHost;
  }

  public int getServicePort() {
    return servicePort;
  }

  public void setServicePort(int servicePort) {
    this.servicePort = servicePort;
  }

  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }
}
