package com.adwaymalhotra.masterslavemeshchat;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adwaymalhotra.masterslavemeshchat.interfaces.MainToServiceInterface;
import com.adwaymalhotra.masterslavemeshchat.interfaces.MessageReceivedListener;
import com.adwaymalhotra.masterslavemeshchat.interfaces.ServiceToMainInterface;
import com.adwaymalhotra.masterslavemeshchat.listeners.CustomResolveListener;
import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;
import com.adwaymalhotra.masterslavemeshchat.util.Constants;
import com.adwaymalhotra.masterslavemeshchat.util.GenericThread;
import com.adwaymalhotra.masterslavemeshchat.util.MessagingThread;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;

/**
 * Created by Adway on 7/30/2016.
 */
public class LocalService extends Service implements MessageReceivedListener, MainToServiceInterface {
  private static final String TAG = "LocalService";
  private ServiceToMainInterface serviceToMainInterface;

  private String mServiceName;

  private NsdManager mNsdManager;
  private NsdManager.RegistrationListener mRegistrationListener;
  private NsdManager.DiscoveryListener mDiscoveryListener;

  private Socket listenerSocket;

  private MessagingThread listenerThread;
  private ArrayList<NodeProfile> listOfAvailableConnections = new ArrayList<>();
  private HashMap<NodeProfile, MessagingThread> mapOfActiveThreads = new HashMap<>();
  private HashMap<NodeProfile, ArrayList<ChatMessage>> mapOfMessages = new HashMap<>();
  private HashMap<NodeProfile, Socket> mapOfSockets = new HashMap<>();

  private Queue<NsdServiceInfo> resolvableQueue = new ArrayDeque<>();

  @Nullable @Override public IBinder onBind(Intent intent) {
    Log.d(TAG, "onBind: ");
    return new InterfaceBinder();
  }

  @Override public void onCreate() {
    super.onCreate();
    Log.d(TAG, "onCreate: ");

    initRegistrationListeners();
    registerService();
    initDiscoveryAndDiscoveryListeners();
  }

  private void registerService() {
    String serviceName = Build.MODEL + Constants.DELIMITER
        + Constants.UserMode.getString(Constants.currentUserMode);
    Constants.myProfile.setServiceName(serviceName);

    NsdServiceInfo serviceInfo = new NsdServiceInfo();
    serviceInfo.setServiceName(serviceName);

    serviceInfo.setServiceType(Constants.SERVICE_TYPE);
    serviceInfo.setPort(Constants.SERVICE_PORT);

    mNsdManager = (NsdManager) getApplicationContext()
        .getSystemService(Context.NSD_SERVICE);

    mNsdManager.registerService(serviceInfo,
        NsdManager.PROTOCOL_DNS_SD, mRegistrationListener);

    new GenericThread(new Runnable() {
      @Override public void run() {
        ServerSocket serverSocket = null;
        try {
          serverSocket = new ServerSocket(Constants.SERVICE_PORT);
          listenerSocket = serverSocket.accept();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }, new GenericThread.ThreadCallback() {
      @Override public void onCompleted() {
        listenerThread = new MessagingThread(listenerSocket, LocalService.this);
        listenerThread.start();
      }
    }).start();
  }

  public void initRegistrationListeners() {
    mRegistrationListener = new NsdManager.RegistrationListener() {

      @Override
      public void onServiceRegistered(NsdServiceInfo nsdServiceInfo) {
        // unlikely scenario where android renames the service
        Log.d(TAG, "onServiceRegistered: ");
        mServiceName = nsdServiceInfo.getServiceName();
        Constants.myProfile.setServiceName(mServiceName);
      }

      @Override
      public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
        Log.d(TAG, "onRegistrationFailed: error code " + errorCode);
      }

      @Override
      public void onServiceUnregistered(NsdServiceInfo arg0) {
        Log.d(TAG, "onServiceUnregistered: ");
      }

      @Override
      public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
        // Unregistration failed!
        Log.d(TAG, "onUnregistrationFailed: error code " + errorCode);
      }
    };
  }

  public void initDiscoveryAndDiscoveryListeners() {

    // Instantiate a new DiscoveryListener
    mDiscoveryListener = new NsdManager.DiscoveryListener() {

      @Override
      public void onDiscoveryStarted(String regType) {
        Log.d(TAG, "Service discovery started");
      }

      @Override
      public void onServiceFound(NsdServiceInfo service) {
        Log.d(TAG, "onServiceFound: " + service);
        if (isValidService(service)) {
          resolvableQueue.add(service);
          if (resolvableQueue.size() == 1) {
            resolveValidService(resolvableQueue.remove());
          }
        }
      }

      @Override
      public void onServiceLost(NsdServiceInfo service) {
        // When the network service is no longer available.
        Log.e(TAG, "service lost " + service);
      }

      @Override
      public void onDiscoveryStopped(String serviceType) {
        Log.i(TAG, "Discovery stopped: " + serviceType);
      }

      @Override
      public void onStartDiscoveryFailed(String serviceType, int errorCode) {
        Log.e(TAG, "Discovery failed: Error code:" + errorCode);
        mNsdManager.stopServiceDiscovery(this);
      }

      @Override
      public void onStopDiscoveryFailed(String serviceType, int errorCode) {
        Log.e(TAG, "Discovery failed: Error code:" + errorCode);
        mNsdManager.stopServiceDiscovery(this);
      }
    };

    //directing NSD Manager to discover available services
    mNsdManager.discoverServices(Constants.SERVICE_TYPE,
        NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
  }

  private boolean isValidService(NsdServiceInfo service) {
    if (Constants.currentUserMode == Constants.UserMode.SLAVE) {
      if (service.getServiceName().contains(Constants.UserMode
          .getString(Constants.UserMode.MASTER))) {
        return true;
      }
    } else if (Constants.currentUserMode == Constants.UserMode.MASTER) {
      return true;
    }
    return false;
  }

  private void resolveValidService(final NsdServiceInfo service) {
    mNsdManager.resolveService(service, new CustomResolveListener(mServiceName,
        new CustomResolveListener.Callback() {
          @Override public void onServiceResolved(NodeProfile nodeProfile) {
            Log.d(TAG, "onServiceResolved: " + nodeProfile.getServiceHost());
            int index = listOfAvailableConnections.indexOf(nodeProfile);
            if (index == -1) {
              listOfAvailableConnections.add(nodeProfile);
            } else {
              listOfAvailableConnections.set(index, nodeProfile);
            }

            serviceToMainInterface.notifyConnectionsListUpdated(listOfAvailableConnections);

            if (resolvableQueue.size() > 0) {
              resolveValidService(resolvableQueue.remove());
            }
          }
        }));
  }

  @Override public void onMessageReceived(String message) {
    Log.d(TAG, "onMessageReceived: " + message);
    ChatMessage chatMessage = new Gson().fromJson(message, ChatMessage.class);
    if (chatMessage.getSender().getUserMode() == Constants.UserMode.MASTER) {
      ArrayList<ChatMessage> messagesWithNode = mapOfMessages.get(chatMessage.getSender());
      if(messagesWithNode == null) {
        messagesWithNode = new ArrayList<>();
      }
      messagesWithNode.add(chatMessage);
      mapOfMessages.put(chatMessage.getSender(), messagesWithNode);

      //notifying main
      serviceToMainInterface.notifyMessageListUpdated(chatMessage.getSender(), messagesWithNode);
    } else if (chatMessage.getSender().getUserMode() == Constants.UserMode.SLAVE) {
      if (Constants.currentUserMode != Constants.UserMode.SLAVE) {
        ArrayList<ChatMessage> messagesWithNode = mapOfMessages.get(chatMessage.getSender());
        if(messagesWithNode == null) {
          messagesWithNode = new ArrayList<>();
        }
        messagesWithNode.add(chatMessage);
        mapOfMessages.put(chatMessage.getSender(), messagesWithNode);

        //notifying main
        serviceToMainInterface.notifyMessageListUpdated(chatMessage.getSender(), messagesWithNode);
      }
    }
  }


  @Override public void connectToNode(final NodeProfile nodeProfile) {
    Log.d(TAG, "connectToNode: ");

    new GenericThread(new Runnable() {
      @Override public void run() {
        try {
          Log.d(TAG, "run: making socket");
          Socket socket = new Socket(nodeProfile.getServiceHost(), nodeProfile.getServicePort());
          mapOfSockets.put(nodeProfile, socket);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }, new GenericThread.ThreadCallback() {
      @Override public void onCompleted() {
        Log.d(TAG, "onCompleted: ");
        MessagingThread thread = new MessagingThread(mapOfSockets.get(nodeProfile), LocalService.this);
        mapOfActiveThreads.put(nodeProfile, thread);
        thread.start();
        serviceToMainInterface.notifyConnectionStatus(nodeProfile, true);
      }
    }).start();
  }

  @Override public void sendMessage(NodeProfile nodeProfile, String message) {
    MessagingThread thread = mapOfActiveThreads.get(nodeProfile);
    ChatMessage chatMessage = new ChatMessage();
    chatMessage.setReceiver(nodeProfile);
    chatMessage.setSender(Constants.myProfile);
    chatMessage.setTimestamp(System.currentTimeMillis());
    chatMessage.setText(message);
    ArrayList<ChatMessage> messages = mapOfMessages.get(nodeProfile);
    if(messages == null){
      messages = new ArrayList<>();
    }
    messages.add(chatMessage);
    mapOfMessages.put(nodeProfile, messages);
    serviceToMainInterface.notifyMessageListUpdated(nodeProfile, messages);
    String json = new Gson().toJson(chatMessage);
    thread.sendMessage(json);
  }

  @Override public void tearDownService() {
    for (MessagingThread t : mapOfActiveThreads.values()) {
      t.terminateThread();
    }
    tearDownService();
  }

  @Override public ArrayList<NodeProfile> getAllContacts() {
    return listOfAvailableConnections;
  }

  @Override public ArrayList<ChatMessage> getAllMessagesForNode(NodeProfile nodeProfile) {
    return mapOfMessages.get(nodeProfile);
  }

  @Override public void refreshDiscoveredServices() {
//    initDiscoveryAndDiscoveryListeners();
  }

  @Override public void onDestroy() {
    tearDownService();
    super.onDestroy();
  }

  public class InterfaceBinder extends Binder {
    public MainToServiceInterface getServiceInterface() {
      return LocalService.this;
    }

    public void setMainInterface(ServiceToMainInterface serviceToMainInterface) {
      LocalService.this.serviceToMainInterface = serviceToMainInterface;
    }
  }
}
