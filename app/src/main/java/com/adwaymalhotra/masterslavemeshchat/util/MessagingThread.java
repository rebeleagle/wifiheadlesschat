package com.adwaymalhotra.masterslavemeshchat.util;

import android.util.Log;

import com.adwaymalhotra.masterslavemeshchat.interfaces.MessageReceivedListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adway on 7/31/2016.
 */
public class MessagingThread extends Thread {
  private static final String TAG = "MessagingThread";

  private Socket socket;
  private MessageReceivedListener messageReceivedListener;

  private InputStream dataInputStream;
  private OutputStream dataOutputStream;

  private List<String> outputMessageBuffer = new ArrayList<>();
  private List<String> inputMessageBuffer = new ArrayList<>();
  private boolean shouldTerminate = false;

  public MessagingThread(Socket socket, MessageReceivedListener listener) {
    this.socket = socket;
    this.messageReceivedListener = listener;
    try {
      dataInputStream = socket.getInputStream();
      dataOutputStream = socket.getOutputStream();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void sendMessage(String message) {
    outputMessageBuffer.add(message);
  }

  public void terminateThread() {
    shouldTerminate = true;
  }

  @Override public void run() {
    super.run();
    try {
      //TODO send identifying message to begin


      while (!shouldTerminate) {
        if (dataInputStream.available() > 0) {
          Log.d(TAG, "run: message received...");
          BufferedReader r = new BufferedReader(new InputStreamReader(dataInputStream));
          StringBuilder total = new StringBuilder();
          String line;
          while ((line = r.readLine()) != null) {
            Log.d(TAG, "run: readline "+ line);
            if (line.equals("EOM"))
              break;
            messageReceivedListener.onMessageReceived(line);
          }
//          String result = total.toString();
//          Log.d(TAG, "run: message: " + result);
//          inputMessageBuffer.add(result);
//          messageReceivedListener.onMessageReceived(result);
        }

//        if (inputMessageBuffer.size() > 0) {
//          Log.d(TAG, "run: for loop starting on input buffer..");
//          for (String s : inputMessageBuffer) {
//            messageReceivedListener.onMessageReceived(s);
//          }
//        }

        if (outputMessageBuffer.size() > 0) {
          for (String msg : outputMessageBuffer) {
            dataOutputStream.write(msg.getBytes());
            dataOutputStream.flush();
            dataOutputStream.write("\n".getBytes());
            dataOutputStream.flush();
          }
          outputMessageBuffer = new ArrayList<>();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
