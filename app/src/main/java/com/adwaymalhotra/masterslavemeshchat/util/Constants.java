package com.adwaymalhotra.masterslavemeshchat.util;

import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

/**
 * Created by Adway on 7/30/2016.
 */
public class Constants {
  public static final String DELIMITER = "#";
  public static NodeProfile myProfile;

  public enum UserMode {
    MASTER, SLAVE;

    public static String getString(UserMode userMode) {
      switch (userMode) {
        case MASTER:
          return "MASTER";
        case SLAVE:
          return "SLAVE";
        default:
          return "";
      }
    }
  }

  public static UserMode currentUserMode = UserMode.SLAVE;

  public static String DEVICE_NAME = "One";

  public static final String SERVICE_TYPE = "_http._tcp.";
  public static final int SERVICE_PORT = 11060;

  public class LocalServiceMessageCodes {
    public static final int RECEIVE_MESSAGES_FOR_NODE = 1;
    public static final int REQUEST_MESSAGES_FOR_NODE = 2;

    public static final int RECEIVE_ALL_NODES = 3;
    public static final int REQUEST_NODES = 4;

    public static final int SEND_MESSAGE_TO_NODE = 5;
    public static final int CONNECT_TO_NODE = 6;
    public static final int RECEIVE_CONNECTION_STATUS_TO_NODE = 7;
  }

  public class Keys {
    public static final String NODE = "node";
    public static final String LIST_OF_MESSAGES = "list_of_messages";
    public static final String SERVICE_TO_MAIN_LISTENER = "service_to_main_listener";
  }
}
