package com.adwaymalhotra.masterslavemeshchat.util;

/**
 * Created by Adway on 7/31/2016.
 */
public class GenericThread extends Thread {

  private final Runnable runnable;
  private final ThreadCallback callback;

  public GenericThread(Runnable runnable, ThreadCallback callback){
    this.runnable = runnable;
    this.callback = callback;
  }

  @Override public void run() {
    runnable.run();
    callback.onCompleted();
  }

  public interface ThreadCallback {
    void onCompleted();
  }
}
