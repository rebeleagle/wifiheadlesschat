package com.adwaymalhotra.masterslavemeshchat.interfaces;

/**
 * Created by Adway on 7/31/2016.
 */
public interface MessageReceivedListener {
  void onMessageReceived(String message);
}
