package com.adwaymalhotra.masterslavemeshchat.interfaces;

import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

import java.util.ArrayList;

/**
 * Created by Adway on 7/30/2016.
 */
public interface IChatView {
  void updateMessageList(NodeProfile nodeProfile, ArrayList<ChatMessage> allMessages);
}
