package com.adwaymalhotra.masterslavemeshchat.interfaces;

import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

import java.util.ArrayList;

/**
 * Created by Adway on 7/31/2016.
 */
public interface MainToServiceInterface {
  void connectToNode(NodeProfile nodeProfile);

  void sendMessage(NodeProfile nodeProfile, String message);

  void tearDownService();

  ArrayList<NodeProfile> getAllContacts();

  ArrayList<ChatMessage> getAllMessagesForNode(NodeProfile nodeProfile);

  void refreshDiscoveredServices();
}
