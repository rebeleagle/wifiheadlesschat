package com.adwaymalhotra.masterslavemeshchat.interfaces;

/**
 * Created by Adway on 7/30/2016.
 */
public interface ILoginView {
  String getUsername();
  String getPassword();

  void onLoginSuccessful();
  void showErrorMessage(String message);
}
