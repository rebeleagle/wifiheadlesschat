package com.adwaymalhotra.masterslavemeshchat.interfaces;

import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Adway on 7/31/2016.
 */
public interface IParentView {
  void initToolbar(String title, String subtitle);

  void initToolbar(String title);

  void initToolbar(String title, String subtitle, boolean showBackButton);

  void initToolbar(String title, boolean showBackButton);

  void startChatWith(NodeProfile nodeProfile);

  void gotoContactsView();

  ArrayList<ChatMessage> getAllMessagesForNode(NodeProfile nodeProfile);

  ArrayList<NodeProfile> getAllContacts();

  void setChatView(IChatView chatView);

  void setContactsView(IContactView contactsView);

  void refreshContacts();

  void sendMessage(String message, NodeProfile nodeProfile);
}
