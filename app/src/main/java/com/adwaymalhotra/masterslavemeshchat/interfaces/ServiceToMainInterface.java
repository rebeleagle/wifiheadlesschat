package com.adwaymalhotra.masterslavemeshchat.interfaces;

import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

import java.util.ArrayList;

/**
 * Created by Adway on 7/31/2016.
 */
public interface ServiceToMainInterface {

  void notifyConnectionStatus(NodeProfile nodeProfile, boolean status);

  void notifyConnectionsListUpdated(ArrayList<NodeProfile> list);

  void notifyMessageListUpdated(NodeProfile nodeProfile, ArrayList<ChatMessage> allMessages);

  void showSnackbar(String message);

  void showToast(String message);
}
