package com.adwaymalhotra.masterslavemeshchat.adapters;

import android.support.v4.text.TextDirectionHeuristicCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adwaymalhotra.masterslavemeshchat.R;
import com.adwaymalhotra.masterslavemeshchat.model.NodeProfile;

import org.w3c.dom.Node;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Adway on 7/31/2016.
 */
public class ContactsRecyclerAdapter extends
    RecyclerView.Adapter<ContactsRecyclerAdapter.ContactsViewHolder> {

  ArrayList<NodeProfile> allNodes = new ArrayList<>();

  public ContactsRecyclerAdapter(ArrayList<NodeProfile> allNodes) {
    this.allNodes = allNodes;
  }

  @Override
  public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.row_contact, parent, false);
    return new ContactsViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ContactsViewHolder holder, int position) {
    NodeProfile nodeProfile = allNodes.get(position);
    holder.nameTV.setText(nodeProfile.getNodeName());
  }

  @Override public int getItemCount() {
    return allNodes.size();
  }

  public void updateDataset(ArrayList<NodeProfile> listOfNodes) {
    allNodes = listOfNodes;
    notifyDataSetChanged();
  }

  public NodeProfile getItemAt(int position) {
    return allNodes.get(position);
  }

  public static class ContactsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.row_contact_name) public TextView nameTV;

    public ContactsViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
