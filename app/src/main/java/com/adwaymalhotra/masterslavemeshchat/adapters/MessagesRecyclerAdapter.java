package com.adwaymalhotra.masterslavemeshchat.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adwaymalhotra.masterslavemeshchat.R;
import com.adwaymalhotra.masterslavemeshchat.model.ChatMessage;
import com.adwaymalhotra.masterslavemeshchat.util.Constants;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Adway on 7/31/2016.
 */
public class MessagesRecyclerAdapter extends
    RecyclerView.Adapter<MessagesRecyclerAdapter.MessagesViewHolder> {

  ArrayList<ChatMessage> allMessages = new ArrayList<>();

  public MessagesRecyclerAdapter(ArrayList<ChatMessage> allMessages) {
    this.allMessages = allMessages;
    Collections.sort(allMessages);
  }

  @Override
  public MessagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    int layout = viewType == 0 ? R.layout.row_message_mine : R.layout.row_message_their;
    View view = LayoutInflater.from(parent.getContext())
        .inflate(layout, parent, false);
    return new MessagesViewHolder(view);
  }

  @Override
  public void onBindViewHolder(MessagesViewHolder holder, int position) {
    ChatMessage message = allMessages.get(position);
    holder.messageText.setText(message.getText());
  }

  @Override public int getItemCount() {
    return allMessages.size();
  }

  @Override public int getItemViewType(int position) {
    ChatMessage message = allMessages.get(position);
    if(message.getSender().equals(Constants.myProfile)) return 0;
    else return 1;
  }

  public void updateDataset(ArrayList<ChatMessage> listOfMessages) {
    allMessages = listOfMessages;
    Collections.sort(allMessages);
    notifyDataSetChanged();
  }

  static class MessagesViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text) TextView messageText;

    public MessagesViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
